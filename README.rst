Trio FTPLIB
===========

Welcome to `Trio FTPLIB <Project URL (for setup.py metadata)>`__!

Trionic async version of stdlib's ftplib

License: Your choice of MIT or Apache License 2.0

COOKIECUTTER-TRIO-TODO: finish filling in your README!
Must be valid ReST; also used as the PyPI description.
