from async_generator import aclosing

from trio_ftplib import download_text


async def test_tso_sinaf():
    async with aclosing(download_text(server='ftpplex01.corecaixa', user='ftpopr', password='ftpbsa',
                                      path='MIC.NAF.MZ.BBD2.M130D001.D200306')) as _text:
        async for line in _text:
            assert line.count('@') == 15
            break
