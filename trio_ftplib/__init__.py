from ._aftplib import AFTP
from .operations import download_text
from ftplib import Error as FTPLibError
